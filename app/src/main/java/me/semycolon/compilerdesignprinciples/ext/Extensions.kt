package me.semycolon.compilerdesignprinciples.ext

import android.util.Base64

fun encodeB64(input : String) : String{
    val encodeToString = Base64.encodeToString(input.toByteArray(), Base64.DEFAULT)
    return encodeToString
}

fun getCurrentMethod() : String {
    val name : String = Thread.currentThread().stackTrace[0].toString()
    if (name.isEmpty()){
        return "Method Not found!!"
    }
    return name
}