package me.semycolon.compilerdesignprinciples.regx_2_nfa

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_regx_2_nfa.*
import me.semycolon.compilerdesignprinciples.R
import me.semycolon.compilerdesignprinciples.ext.encodeB64

class RegexToNfaFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_regx_2_nfa,container,false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        output.settings.javaScriptEnabled = true


        convert_btn.setOnClickListener {
            //get the regex and convert it to some real shit!!!
            val input = input.text.toString()
            val decodedInput = encodeB64(input)
            // file:///android_asset/

            output.loadUrl("file:///android_asset/www/regex2nfa.html?regex=$decodedInput")

        }
    }
}