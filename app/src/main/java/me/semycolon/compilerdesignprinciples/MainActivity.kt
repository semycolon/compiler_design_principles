package me.semycolon.compilerdesignprinciples

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import me.semycolon.compilerdesignprinciples.ext.encodeB64
import me.semycolon.compilerdesignprinciples.ext.getCurrentMethod
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.info
import kotlin.properties.Delegates
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_developer.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, AnkoLogger {


    var lastestUrl = ""
    val WWW = "file:///android_asset/www/"
    private val html_regex_2_nfa = "regex2nfa.html?regex="
    private val html_regex_2_dfa = "nfa2dfa.html?regex="
    private val html_left_fact = "left_fact.html?grammar="
    private val html_left_rec = "left_rec.html?grammar="
    private val html_cfg_2_ll = "cfg2ll.html?grammar="
    private val html_first_follow = "first_follow.html?grammar="
    private val html_lr0 = "lr0.html?grammar="
    private val html_lr1 = "lr1.html?grammar="
    private val html_lalr = "lalr.html?grammar="
    private val html_ll_1 = "ll1.html?grammar="

    enum class Which {
        REGEX_2_NFA, REGEX_2_DFA, LEFT_FACTORING, LEFT_RECURSION, CFG_2_LLK, FIRST_FOLLOW, LR0_SLR1, LR1, LALR, LL1
    }

    private var which by Delegates.observable(Which.REGEX_2_DFA, { property, oldValue, newValue ->
        debug { "property:$property  oldvalue:$oldValue   nameValue:$newValue" }
        onWhichChanged()
    })

    private fun onWhichChanged() {
        info { "onWhichChanged Called from ${getCurrentMethod()}" }
        //set title
        initTitle()
        //set input type
        if (which == Which.REGEX_2_DFA || which == Which.REGEX_2_NFA) {
            //get a regex
            input.maxLines = 1
        } else {
            //get a grammar
            input.maxLines = 8
        }

        //clear input
        input.setText("")
        input.hint = getHint()
        output_div.removeAllViews()
    }

    private fun initTitle() {
        supportActionBar?.title = getTextForTitle()
    }

    private fun getTextForTitle(): String {
        return when (which) {
            Which.REGEX_2_NFA -> "Regex to NFA"
            Which.REGEX_2_DFA -> "Regex to DFA"
            Which.LEFT_RECURSION -> "Left Recursion"
            Which.LEFT_FACTORING -> "Left Factoring"
            Which.FIRST_FOLLOW -> "First & Follow"
            Which.LR1 -> "LR(1)"
            Which.LR0_SLR1 -> "LR(0) / SLR(0)"
            Which.CFG_2_LLK -> "CFG to LL(K)"
            Which.LALR -> "LALR"
            Which.LL1 -> "LL(1)"
        }
    }  private fun getHint(): String {
        return when (which) {
            Which.REGEX_2_NFA  , Which.REGEX_2_DFA-> "((ϵ|a)b*)*"
            Which.FIRST_FOLLOW,
            Which.LEFT_RECURSION
                ,Which.LEFT_FACTORING , Which.CFG_2_LLK-> "A -> A c | A a d | b d | ϵ"

            Which.LR1,Which.LALR -> "S -> C C\n" +
                    "C -> c C | d\n"


            Which.LR0_SLR1 -> "E -> E + T | T\n" +
                    "T -> T * F | F\n" +
                    "F -> ( E ) | id\n"

            Which.LL1 -> "E -> T E'\n" +
                    "E' -> + T E' | ϵ\n" +
                    "T -> F T'\n" +
                    "T' -> * F T' | ϵ\n" +
                    "F -> ( E ) | id\n"
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)


        initOnce()
        onWhichChanged()
    }

    private fun initOnce() {
        //hide clear btn
        input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isEmpty()) {
                    clear_btn.visibility = View.GONE
                } else {
                    clear_btn.visibility = View.VISIBLE
                }
            }
        })
        clear_btn.setOnClickListener { input.setText("") }
        convert_btn.setOnClickListener {
            if (input.text.isNotEmpty()) {
                onCalculate(input.text.toString())
                //hide keyboard
                try {
                    val view = this.currentFocus
                    if (view != null) {
                        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(view.windowToken, 0)
                    }
                } catch (e: Exception) { e.printStackTrace()                }
            }

        else Snackbar.make(output_div,"input is empty !",Snackbar.LENGTH_SHORT).show() }


        fullscreen_btn.visibility = View.GONE
        val logo = nav_view.getHeaderView(0).findViewById<ImageView>(R.id.nav_logo_image)
        logo.setOnClickListener {
            val anim = AnimationUtils.loadAnimation(this, R.anim.rotation)
            anim.interpolator = AccelerateDecelerateInterpolator()
            logo.startAnimation(anim)
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun onCalculate(inputText: String) {
        var url = ""
        val htmlFile = when (which) {
            Which.REGEX_2_NFA -> html_regex_2_nfa
            Which.REGEX_2_DFA -> html_regex_2_dfa
            Which.LEFT_FACTORING -> html_left_fact
            Which.LEFT_RECURSION -> html_left_rec
            Which.CFG_2_LLK -> html_cfg_2_ll
            Which.FIRST_FOLLOW -> html_first_follow
            Which.LR0_SLR1 -> html_lr0
            Which.LR1 -> html_lr1
            Which.LALR -> html_lalr
            Which.LL1 -> html_ll_1

            else -> {
                "else some shit"
            }
        }
        info("current which $which")
        url = WWW + htmlFile + encodeB64(inputText)
        info("url $url    isnull:${url.isEmpty()}")
        lastestUrl = url

        output_div.removeAllViews()
        val output = WebView(this)
        output.settings.javaScriptEnabled = true
        output.loadUrl(url)

        output_div.addView(output)

        fullscreen_btn.visibility = View.VISIBLE
        fullscreen_btn.setOnClickListener{
            val intent = Intent(this, FullscreenActivity::class.java).putExtra("url", url).putExtra("title",inputText)
            try {
                startActivity(intent)
            } catch (e: Exception) {e.printStackTrace()            }

        }
    }


    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.menu_main_epsilon -> {
                input.append("ϵ")
                return true
            }
            R.id.menu_main_info ->{
                //start info Activity
                val intent = Intent(this@MainActivity, InfoActivity::class.java)
                val supported_grammar_text = getString(R.string.supported_grammar_text)
                val supported_regex_text = getString(R.string.supported_regex_text)
                val regex_example = getString(R.string.regex_example)
                val grammar_example = getString(R.string.grammar_example)
                when(which){
                    Which.REGEX_2_DFA ->{
                        intent.putExtra("intro",getString(R.string.intro_dfa))
                    }
                    Which.REGEX_2_NFA -> {
                        intent.putExtra("intro",getString(R.string.intro_nfa))
                    }
                    Which.LEFT_RECURSION ->{
                        intent.putExtra("intro",getString(R.string.intro_left_rec))
                    }
                    Which.LEFT_FACTORING ->{
                        intent.putExtra("intro",getString(R.string.left_fact))
                    }
                    Which.CFG_2_LLK ->{
                        intent.putExtra("intro",getString(R.string.intro_cfg_llk))
                    }
                    Which.LL1 ->{
                        intent.putExtra("intro",getString(R.string.intro_ll1))
                    }
                    Which.LALR ->{
                        intent.putExtra("intro",getString(R.string.intro_lalr))
                    }
                    Which.LR1 ->{
                        intent.putExtra("intro",getString(R.string.lr1))
                    }
                    Which.LR0_SLR1 ->{
                        intent.putExtra("intro",getString(R.string.intro_lr0_slr))
                    }
                    Which.FIRST_FOLLOW ->{
                        intent.putExtra("intro",getString(R.string.intro_f_f))
                    }

                }

                when(which){
                    Which.REGEX_2_NFA,Which.REGEX_2_DFA ->{
                        intent.putExtra("supported_text",supported_regex_text)
                        intent.putExtra("example",regex_example)
                    }
                    else -> {
                        intent.putExtra("supported_text",supported_grammar_text)
                        intent.putExtra("example",grammar_example)
                    }
                }

                intent.putExtra("title",getTextForTitle())

                try {
                    startActivity(intent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_regex_to_nfa -> {
                which = Which.REGEX_2_NFA
            }
            R.id.nav_regex_to_dfa -> {
                which = Which.REGEX_2_DFA
            }
            R.id.nav_cfg_to_ll_k -> {
                which = Which.CFG_2_LLK
            }
            R.id.nav_first_follow -> {
                which = Which.FIRST_FOLLOW
            }
            R.id.nav_la_lr -> {
                which = Which.LALR
            }
            R.id.nav_left_factoring -> {
                which = Which.LEFT_FACTORING
            }
            R.id.nav_left_recursion -> {
                which = Which.LEFT_RECURSION
            }
            R.id.nav_lr_0_slr_1 -> {
                which = Which.LR0_SLR1
            }
            R.id.nav_lr_1 -> {
                which = Which.LR1
            }
            R.id.nav_developer ->{
                try {
                    startActivity(Intent(this,DeveloperActivity::class.java))
                } catch (e: Exception) {
                }
            }
            R.id.nav_ll_1 -> {
                which = Which.LL1
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
