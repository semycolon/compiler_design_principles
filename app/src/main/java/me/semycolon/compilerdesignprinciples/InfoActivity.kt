package me.semycolon.compilerdesignprinciples

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_info.*

class InfoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        try {
            info_txt_intro.text = intent.extras.getString("intro")
            info_txt_supported.text = getString(R.string.text_supported_grammars)
            info_txt_supported_text.text = intent.extras.getString("supported_text")
            info_txt_example_text.text = intent.extras.getString("example")
            supportActionBar?.title = intent.extras.getString("title")

            supportActionBar?.setDisplayShowHomeEnabled(true)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        } catch (e: Exception) { e.printStackTrace() ; finish() }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
