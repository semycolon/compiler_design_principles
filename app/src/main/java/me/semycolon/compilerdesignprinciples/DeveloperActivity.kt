package me.semycolon.compilerdesignprinciples

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_developer.*
import android.content.ActivityNotFoundException
import android.view.MenuItem
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AccelerateInterpolator
import android.view.animation.AnimationUtils


class DeveloperActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_developer)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = "Developer"

        dev_name.text = "Syed Hassan Asheghi"
        dev_instagram.text = "SemyColon_"
        dev_telegram.text = "@SemyColon"
        dev_mail.text = "info@SemyColon.me"

        dev_name.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW,Uri.parse("http://semycolon.me"))
            try {
                startActivity(intent)

                startActivity(Intent.createChooser(intent, "goto SemyColon.me"))
            } catch (e: Exception) {
            }
        }

        dev_mail.setOnClickListener {

            val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "info@semycolon.me", null))
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Compiler App")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "")
            try {
                startActivity(Intent.createChooser(emailIntent, "Send email with"))
            } catch (e: Exception) {
            }
        }

        dev_instagram.setOnClickListener {
            val uri = Uri.parse("http://instagram.com/_u/semycolon_")
            val likeIng = Intent(Intent.ACTION_VIEW, uri)

            likeIng.`package` = "com.instagram.android"

            try {
                startActivity(likeIng)
            } catch (e: ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://instagram.com/semycolon")))
            }

        }

        dev_telegram.setOnClickListener {
            try {
                startActivity(Intent(Intent.ACTION_VIEW,Uri.parse("https://telegram.me/semycolon")))
            } catch (e: Exception) {
            }
        }

        logo_image.setOnClickListener {
            val anim = AnimationUtils.loadAnimation(this, R.anim.rotation)
            anim.interpolator = AccelerateDecelerateInterpolator()
            logo_image.startAnimation(anim)
        }

        dev_bazaar.setOnClickListener {
            val DEVELOPER_ID = "semycolon"
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("bazaar://collection?slug=by_author&aid=$DEVELOPER_ID")
            intent.`package` = "com.farsitel.bazaar"
            try {
                startActivity(intent)
            } catch (e: Exception) {
                startActivity(Intent(Intent.ACTION_VIEW,Uri.parse("https://cafebazaar.ir/developer/semycolon/")))
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
